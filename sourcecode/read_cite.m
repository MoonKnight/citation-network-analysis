%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%read in Cit-HepPh.txt
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fid=fopen('../data/original_files/hep-th-citations');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% old method, not efficient for sparse matrix in matlab 
% also try to resolve the miss typed links in standford version
% but it is no longer needed for kdd2003 version of the dataset
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Cit-HepTh.txt contains 352807 edges. But only about 50,000 of the edges
% % have both nodes inside the Cit-HepTh-date.txt.
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % tmpstring=fscanf(fid, '# Directed graph (each unordered pair of nodes is saved once): Cit-HepTh.txt\n');
% % tmpstring=fscanf(fid, '# Paper citation network of Arxiv High Energy Physics Theory category\n');
% % tmpstring=fscanf(fid, '# Nodes: 27770 Edges: 352807\n');
% % tmpstring=fscanf(fid, '# FromNodeId	ToNodeId\n');
% % tmpstring=fscanf(fid, '# Directed graph (each unordered pair of nodes is saved once): Cit-HepPh.txt\n');
% % tmpstring=fscanf(fid, '# Paper citation network of Arxiv High Energy Physics category\n');
% % tmpstring=fscanf(fid, '# Nodes: 34546 Edges: 421578\n');
% % % 347268 outof 421578 edges have both ends within the Cit-HepTh-date.txt
% % tmpstring=fscanf(fid, '# FromNodeId	ToNodeId\n');
% 
% % Cit_Matrix=sparse(num_nodes,num_nodes);
% % citefrom=zeros(num_nodes,1);
% % cited = sparse(num_nodes,max(month));
% % cited_timeprofile=zeros(num_nodes,max(month));
% 
% % i=1;
% % %i0=1;
% % j=1;
% % k=1;
% % while (~feof(fid));
% %     tmpint(1:2)=fscanf(fid, '%d %d\n',[1 2]);
% %     fromid=pointer(tmpint(1));
% %     toid=pointer(tmpint(2));
% %     
% %     % if one of the nodes are not included in the paper list
% %     if((fromid==0) || (toid==0))
% %         if(fromid==0)
% %             recordmissing(i)=tmpint(1);
% %             i=i+1;
% %         end
% %         if(toid==0)
% %             recordmissing(i)=tmpint(2);
% %             i=i+1;
% %         end
% %         i0=i0+1;
% %     else
% %         Cit_Matrix(fromid,toid)=1;
% %         j=j+1;
% %         month_cited=month(fromid);
% %         cited(toid,month_cited)=cited(toid,month_cited)+1;
% %         citefrom(fromid)=citefrom(fromid)+1;
% %     end
% %     k=k+1;
% %     
% %     if (mod(k,5000)==0)
% %         fprintf('%7d links established\n',k);
% %     end
% %     
% end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% new version
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% estimate the total number of links
% assuming about 10 refs per paper
num_links=num_nodes*10 ;
citeI=zeros(1,num_links);
citeJ=zeros(1,num_links);

timeprofileI=zeros(1,num_links);
timeprofileJ=zeros(1,num_links);

k=1;
tic;
while (~feof(fid));
    tmpint(1:2)=fscanf(fid,'%d %d\n',[1 2]);
    fromid=pointer(tmpint(1));
    toid=pointer(tmpint(2));
    
    citeI(k)=fromid;
    citeJ(k)=toid;
    
    timeprofileI(k)=toid;
    timeprofileJ(k)=month(fromid);

    % updating progress on the screen
    if (mod(k,50000)==0)
        fprintf('%7d links established in %f seconds\n',k, toc);
    end  
    k=k+1;
    
end
fclose(fid);

num_links=k-1;
citeI=citeI(1:num_links);
citeJ=citeJ(1:num_links);
timeprofileI=timeprofileI(1:num_links);
timeprofileJ=timeprofileJ(1:num_links);

Cit_Matrix=logical(sparse(citeI,citeJ,ones(1,num_links),num_nodes,num_nodes));
Cit_Timeprofile=sparse(timeprofileI,timeprofileJ,ones(1,num_links),num_nodes,max(month));

Citefrom_Total=full(sum(Cit_Matrix,2));
Cited_Total=full(sum(Cit_Matrix));

clear('citeI','citeJ','timeprofileI','timeprofileJ','tmpint');


