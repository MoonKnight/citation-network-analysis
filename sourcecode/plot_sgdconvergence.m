para1=4;
para2=2;

plot(tt5(para1,:),tt5(para2,:),'d-', 'color', [1 .5 0]);
hold on
plot(t1(para1,:),t1(para2,:),'d-');

plot(t2(para1,:),t2(para2,:),'rd-');
plot(t4(para1,:),t4(para2,:),'gd-');
plot(tt3(para1,:),tt3(para2,:),'cd-');
plot(tt1(para1,:),tt1(para2,:),'md-');
plot(t3(para1,:),t3(para2,:),'d-','color',[0 .5 .3]);
plot(theta_fit(para1),theta_fit(para2),'k*','markersize',15.0);
%axis([-20 0 -2 1.5])
hold off
