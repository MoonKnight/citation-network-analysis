load('authors_information.mat');
load('kdd2003_wholedata.mat');
load('author_and_abs.mat');

num_para_range=3:5;

logl=zeros(length(num_para_range),2);
sse=zeros(length(num_para_range),2);
logl_baseline=zeros(max(groupid),2);
sse_baseline=zeros(max(groupid),2);

%Try multiple random initializations,
%because EM_clustering is sometimes unstable
num_repeat=1;

num_comm=2; %number of parameters in the model
min_count=10; %min number of counts that will be considererd in NB_train

for validation_set_number=1:5
    
    fprintf('Start creating training set on citations \n');
    training_create;
    fprintf('Start creating training set on authors and abstracts \n');
    training_creat2;
    
    num_para=3;
    GLM_prepare;
    
               EM_clustering;
            NB_train;
            NB_test;
    
    
    for irepeat=1:num_repeat
        k=1;
        for num_para=num_para_range
           
            
            fprintf('\n Start Clustering: EM num_comm: %d \n', num_comm);
            

            
            GLMtest;
            logl(k,1)=logl(k,1)+ltmp/num_repeat;
            sse(k,1)=sse(k,1)+difftmp^2/num_repeat;
            
            GLMpredict;
            logl(k,2)=logl(k,2)+ltmp/num_repeat;
            sse(k,2)=sse(k,2)+difftmp^2/num_repeat;
            
            k=k+1;
        end
    end
end
output;