%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%train naive Baysien
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
gg=1;
index_ingroup=(group==gg);
index_outgroup=~index_ingroup;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% too noisy if total number of appearance <min_count
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
min_count=15;
tmpindex=(wordcounttotal>min_count);
wordcounttotaltmp=wordcounttotal(tmpindex);
wordcounttmp=wordcount(:,tmpindex);
tokenlisttmp=tokenlist(tmpindex);
V=nnz(tmpindex);

neg=wordcounttmp(index_outgroup,:);
pos=wordcounttmp(index_ingroup,:);

neg_words = sum(sum(neg));
pos_words = sum(sum(pos));
neg_log_prior = log(size(neg,1) / num_nodes);
pos_log_prior = log(size(pos,1) / num_nodes);

neg_log_phi=zeros(1,V);
pos_log_phi=zeros(1,V);

for k=1:V,
    neg_log_phi(k) = log((sum(neg(:,k)) + 1) / (neg_words + V));
    pos_log_phi(k) = log((sum(pos(:,k)) + 1) / (pos_words + V));
end

diff=pos_log_phi-neg_log_phi;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Display the 5 most positive words and 5 most negative words
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[sortedValues,sortIndex] = sort(diff,'descend');
maxIndex = sortIndex(1:5);
minIndex = sortIndex(end-4:end);

tokenlisttmp(maxIndex)
diff(maxIndex)
tmpmatrix(1,:)=wordcounttotaltmp(maxIndex);
tmpmatrix(2,:)=sum(pos(:,maxIndex));
full(tmpmatrix)

tokenlisttmp(minIndex)
diff(minIndex)
tmpmatrix(1,:)=wordcounttotaltmp(minIndex);
tmpmatrix(2,:)=sum(pos(:,minIndex));
full(tmpmatrix)
%prob_ingroup=(sum(wordcount(index_ingroup))+1.0)/(number_ingroup+2.0);
%prob_outgroup=(sum(wordcount(index_outgroup))+1.0)/(number_outgroup+2.0);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Prediction on Validation Set
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

testMatrix=wordcount(index_valid,tmpindex);
numTestDocs=nnz(index_valid);


