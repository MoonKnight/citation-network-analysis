posterior=zeros(num_comm,1);
group_valid_overlap=sparse(num_nodes,num_comm);

for i=1:numTestDocs
    [i0,j0,v0]=find(testMatrix(i,:));
    for gg=1:num_comm
        posterior(gg)=sum(v0.*pos_log_phi(gg,j0))+log_prior(1,gg)-sum(v0.*neg_log_phi(gg,j0))-log_prior(2,gg);
    end
    
    tmpindex=(posterior>0);
    group_valid_overlap(i,tmpindex)=1;    
end

