posterior=zeros(num_comm,1);
group_valid=zeros(numTestDocs,1);

for i=1:numTestDocs
    [i0,j0,v0]=find(testMatrix(i,:));
    for gg=1:num_comm
        posterior(gg)=sum(v0.*log_phi(gg,j0))+log_prior(gg);
    end
    
    [maxvlue,maxposition]=max(posterior);
    group_valid(i)=maxposition;    
end

