% Final test on the link-community 
% 
load('authors_information.mat');
 load('kdd2003_wholedata.mat');
 load('author_and_abs.mat');
 load('kdd2003.mat');
 
 
 
 indexrange=1:40:2001;
 % 1 for EM clustering
 % 2 for modularity clustering
 % 3 for directional-modularity clustering
 % 4 for link-clustering
 
 logl=zeros(length(indexrange),2);
 sse=zeros(length(indexrange),2);
 logl_baseline=zeros(1,2);
 sse_baseline=zeros(1,2);
 rel_log=zeros(length(indexrange),2);
 rel_sse=zeros(length(indexrange),2);

 validation_set_number=0;
 num_para=5;
 min_count=10;
 
 index_valid=(groupid==validation_set_number);
 index_filter=(mod(1:num_nodes00,4)==0);
 index_train=((groupid>0).*index_filter');
 index_train=logical(index_train);
 
 
 fprintf('Start creating training set on citations \n');
 training_create;
 fprintf('Start creating training set on authors and abstracts \n');
 training_creat2;
 
 
 GLM_prepare;
 predict0=exp(tmpeta1);
 logl_baseline(2) = citefrom_valid'*tmpeta1-sum(exp(tmpeta1));
 sse_baseline(2) = norm(citefrom_valid-exp(tmpeta1))^2;

 fit0=exp(tmpeta);
 logl_baseline(1) = citefrom'*tmpeta-sum(exp(tmpeta));
 sse_baseline(1) = norm(citefrom-exp(tmpeta))^2
 
 k_para=1;
 for index00=indexrange
     fprintf('index00: %d, similarity %f, PD: %f \n',index00,similar_short(index00),pd_short(index00));
     node_clustering;
     
     GLMtest_link;
     logl(k_para,1)=logl(k_para,1)+ltmp;
     sse(k_para,1)=sse(k_para,1)+difftmp^2;
     
     NB_train_overlap;
     NB_test_overlap;
     
     GLMpredict_link;
     logl(k_para,2)=logl(k_para,2)+ltmp;
     sse(k_para,2)=sse(k_para,2)+difftmp^2;
         
     k_para=k_para+1;
 end
 