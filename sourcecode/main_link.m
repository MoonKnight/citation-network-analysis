load('kdd2003.mat','Cit_Matrix00');
load('kdd2003.mat','groupid');
num_nodes=size(Cit_Matrix00,1);
testindex=(mod(1:num_nodes,4)==0);
testindex=logical(testindex'.*(groupid>0));
Cit_Matrix=Cit_Matrix00(testindex,testindex);

symm2=Cit_Matrix+Cit_Matrix';
symm2(symm2==2)=1;

fprintf('number of nodes: %d \n',size(symm2,1));
tic;
link_clustering;
toc

