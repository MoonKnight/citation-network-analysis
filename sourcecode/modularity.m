%load('kdd2003.mat');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% create modularity matrix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ModMatrix=full(Cit_Matrix+Cit_Matrix');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 21 papers cite themselves ...
% 245 pair of papers cite each other ...
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ModMatrix(ModMatrix==2)=1;
ModMatrix00=ModMatrix;
totallink=full(sum(ModMatrix));
totalm = sum(totallink)/2;

ModMatrix=ModMatrix - (totallink'*totallink)/totalm/2;
% 
% testvector=rand(num_nodes,1);
% %testvector0=ones(num_nodes,1);
% 
% diff_in_dir=100;
% threshold=0.00000001;
% itemax=100;
% 
% for ite=1:itemax
%     testvector0=testvector;
%     testvector=ModMatrix*testvector;
%     diff_in_dir = norm(testvector/norm(testvector) - testvector0/norm(testvector0));
%     fprintf('diff in iteration %d is %e.\n', ite, diff_in_dir);
% end

ModMatrix0=ModMatrix;
group_mod=ones(num_nodes,1);
groupsize=zeros(300,1);
num_comm=1;
%itemax=13;
nonseparable=zeros(300,1); 
ModMatrix=ModMatrix0;

eigen0count=0;
smallgroupcount=0;

tic;
k0=0;
while(sum(nonseparable)<num_comm)
    num_comm0=num_comm;
    for k=1:num_comm0
        if(nonseparable(k)==1)
            continue;
        end
        tmpindex0=(group_mod==k);
        [tmpI,tmpJ,tmpK]=find(tmpindex0);

        if(num_comm>1)
            tmpMatrix=ModMatrix00(tmpindex0,tmpindex0);
            tmpvector2=sum(tmpMatrix);
            tmpvector1=totallink(tmpindex0);
            tmpd=sum(tmpvector1);
            tmpvector0=tmpvector2-tmpvector1*tmpd/2/totalm;
            ModMatrix=ModMatrix0(tmpindex0,tmpindex0)-diag(tmpvector0);
        end
 %       toc 
        
        [tmpvector,tmpvalue]=eigs(ModMatrix,1,'la');
  %      toc
        
        tmpindex=(tmpvector<0);
   %     toc
   
        tmpcount=nnz(tmpindex);
        tmpcount=min(tmpcount,nnz(tmpindex0)-tmpcount);
        
        %threshold=10;
   
        
        if((tmpvalue>1e-10) && (tmpcount>threshold))
            num_comm=num_comm+1;
            tmpindex=(tmpvector<0);
            tmpI=tmpI(tmpindex);
            tmpJ=tmpJ(tmpindex);
            tmpK=tmpK(tmpindex);
            tmpindex2=sparse(tmpI,tmpJ,tmpK,num_nodes,1);
            group_mod(tmpindex2)=num_comm;
  %          fprintf('i: %d, num_comm: %d time: %f eigenvalue: %f size: %d\n',k,num_comm,toc, tmpvalue,length(tmpI));
        else
            nonseparable(k)=1;
            groupsize(k)=length(tmpI);
            if(~tmpvalue>1e-10)
                eigen0count=eigen0count +1;
            else
                smallgroupcount=smallgroupcount+1;
            end
        end
         
    end
    k0=k0+1;
    fprintf('k0: %d, num_comm: %d time: %f\n',k0,num_comm,toc);%, tmpvalue,length(tmpI));   
end

groupsize=groupsize(1:num_comm);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%intergroup connection
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

connectivity=zeros(num_comm,1);

for k=1:num_comm
    tmpindex=(group_mod==k);
    
    % within community links
    tmpMatrix=ModMatrix00(tmpindex,tmpindex);
    totallink_g=sum(sum(tmpMatrix));
        
    connectivity(k)=totallink_g*2/(groupsize(k)*(groupsize(k)-1));
end

fprintf('Size limit: %d, Eigenvalue limit: %d \n', smallgroupcount, eigen0count);
