I=[1,1,2,3,4,5,5,6,6,7,8,8,9,10,11,11,12,13,14,14,1,10];
J=[6,3,6,6,6,10,11,11,13,13,14,9,15,17,17,18,18,16,15,16,7,11];
K=ones(length(I),1);

sparseMatrix=sparse(I,J,K,18,18);
test=full(sparseMatrix);

Cit_Matrix=test;
num_nodes=size(test,1);

threshold=0;

modularity;
group1=group_mod;

modularity_dir;
group2=group_mod;

symm=Cit_Matrix + Cit_Matrix';
[testX,testY,testZ]=k_clique(3,symm);

link_clustering;