%% read_dates
fprintf('Start reading hep-th-slacdates\n');
read_dates;

%% read citations
fprintf('Start reading hep-th-citations\n');
read_cite;

%% read abstracts and authors
fprintf('Start reading authors and abstracts\n');
read_abs;

 
%*****************************************
%% STORE DATA
%*****************************************
num_nodes00=num_nodes;
num_month00=num_month;
num_links00=num_links;
num_author00=num_author;

Month00=month;
Year00=year;
Pointer00=pointer;
ID00=id;

Cit_Matrix00=Cit_Matrix;
Cited_Total00=Cited_Total;
Citefrom_Total00=Citefrom_Total;
Cit_Timeprofile00=Cit_Timeprofile;

Authorlist00=Authorlist;
Author_Matrix00=Author_Matrix;
Authornumbers00=Authornumbers;

Tokenlist00=Tokenlist;
Wordcount_Matrix00=Wordcount_Matrix;
Wordcount_Total00=Wordcount_Total;

save('../data/matlab_format/wholedata.mat','num_nodes00',...
    'num_month00','num_links00','num_author00', 'Month00', 'Year00',...
    'Pointer00', 'ID00', 'Cit_Matrix00', 'Cited_Total00',...
    'Citefrom_Total00','Cit_Timeprofile00', 'Authorlist00',...
    'Author_Matrix00', 'Authornumbers00', 'Tokenlist00', ...
    'Wordcount_Matrix00', 'Wordcount_Total00');