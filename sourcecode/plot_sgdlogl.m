
%plot(ll5,'d-', 'color', [1 .5 0]);
hold on
plot(ll5,'-');

plot(ll4,'r-');
plot(ll2,'g-');
plot(ll3,'c-');
plot(ll1,'m-');
%plot(l3,'d-','color',[0 .5 .3]);
%plot(theta_fit(para1),theta_fit(para2),'k*','markersize',15.0);
tmpeta=Xmatrix*theta_fit;
ltmp=citefrom'*tmpeta-sum(exp(tmpeta));
x0=[0 101];
l0=[ltmp ltmp];
plot(x0, l0, 'k--');
%xlabel('number of iterations');
%ylabel('log likelihood');
axis([0 101 2.3e5, 2.5e5])

hold off
