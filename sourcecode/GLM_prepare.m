%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% prepare variables for GLM fit
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% load('authors_information.mat');
% load('EM_cluster4.mat', 'group');
% load('EM_cluster4.mat', 'num_comm');
% load('kdd2003.mat');
%load('kdd2003.mat','num_nodes');
%load('kdd2003.mat','num_month');
%load('kdd2003.mat','Cit_Matrix');
%load('kdd2003.mat','month');
%load('kdd2003.mat','index_train');
%load('kdd2003.mat','citedtotal');
%load('kdd2003.mat','cited');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% for the training set
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
sameauthor=sameauthor00(index_train,index_train);

earlier=zeros(num_nodes,1);
earlier_sameauthor=zeros(num_nodes,1);
earlier_importance=zeros(num_nodes,1);
earlier_samegroup=zeros(num_nodes,1);

citefrom=full(sum(Cit_Matrix,2));

importance=log((citedtotal+0.5)./(num_month-month+0.5));
importance=importance-min(importance);

fprintf('Generating Variable matrix for GLM fit. \n')

tic;
for i=1:num_nodes
    tmp=(month<month(i));
    earlier_sameauthor(i)=sum(tmp.*sameauthor(i,:)');   % very time-consuming
    earlier_importance(i)=sum(tmp.*importance);
    earlier(i)=sum(tmp);
    if(mod(i,3000)==0)
         fprintf('portion: %f, time %f s \n',i/num_nodes,toc);
    end 
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% for the validation set
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
sameauthor_valid=sameauthor00(index_valid,index_train);
month_valid=month00(index_valid);
num_valid=nnz(index_valid);
Cit_Matrix_valid=Cit_Matrix00(index_valid,index_train);
citefrom_valid=full(sum(Cit_Matrix_valid,2));

earl1=zeros(num_valid,1);
earl1_sameauthor=zeros(num_valid,1);
earl1_importance=zeros(num_valid,1);
earl1_samegroup=zeros(num_valid,1);

fprintf('Generating Variable matrix for the validation set. \n')

tic;
for i=1:num_valid
    tmp1=(month<month_valid(i));
    earl1_sameauthor(i)=sum(tmp1.*sameauthor_valid(i,:)');   % very time-consuming
    earl1_importance(i)=sum(tmp1.*importance);
    earl1(i)=sum(tmp1);
    if(mod(i,1000)==0)
         fprintf('portion: %f, time %f s \n',i/num_valid,toc);
    end 
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% fit baseline model
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Xmatrix=zeros(num_nodes,num_para-1);
Xmatrix(:,1)=1.0;
Xmatrix(:,2)=log(earlier_sameauthor+1);
if(num_para>3)
    Xmatrix(:,3)=earlier_importance/10^4;
    if(num_para>4)
        Xmatrix(:,4)=log(earlier+1.0);
    end
end

test=Xmatrix(:,2:num_para-1);
theta_baseline=glmfit(test,citefrom,'poisson');
tmpeta=Xmatrix*theta_baseline;
% logl_baseline(validation_set_number,1)= citefrom'*tmpeta-sum(exp(tmpeta));
% sse_baseline(validation_set_number,1)=norm(citefrom-exp(tmpeta))^2;

Xvalid=zeros(num_valid,num_para-1);
Xvalid(:,1)=1.0;
Xvalid(:,2)=log(earl1_sameauthor+1);
if(num_para>3)
    Xvalid(:,3)=earl1_importance/10^4;
    if(num_para>4)
        Xvalid(:,4)=log(earl1+1.0);
    end
end

tmpeta1=Xvalid*theta_baseline;
% logl_baseline(validation_set_number,2) = citefrom_valid'*tmpeta1-sum(exp(tmpeta1));
% sse_baseline(validation_set_number,2) = norm(citefrom_valid-exp(tmpeta1))^2;
% 


