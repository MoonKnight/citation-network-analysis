rel_log(:,1)=logl(:,1)/sum(logl_baseline(:,1))-1;
rel_log(:,2)=logl(:,2)/sum(logl_baseline(:,2))-1;
rel_sse(:,1)=sse(:,1)/sum(sse_baseline(:,1))-1;
rel_sse(:,2)=sse(:,2)/sum(sse_baseline(:,2))-1;

%horizontal=2:6;

hFig = figure(1);
set(hFig, 'Position', [0 0 1000 500]);
 
%relative change in loglikelihood
subplot(1,2,1);
%figure;
plot(num_comm_range,rel_log*100,'d-');
xlabel('number of parameters','Fontsize',15);
ylabel('percentage improvement in loglikelihood', 'Fontsize',15);
legend('training set','validation set');


%convergence of iteration
subplot(1,2,2);
plot(num_comm_range,-rel_sse*100,'d-');
xlabel('number of parameters','Fontsize',15);
ylabel('percentage improvement in sum of square errors','Fontsize',15);
legend('training set','validation set');