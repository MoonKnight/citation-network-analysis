%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GLM predict
% use the model fitted previously to predict how many citations a test
% docuument make
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



index_group=sparse(num_nodes,num_comm);
for gg=1:num_comm
    index_group(:,gg)=(group==gg);
end
%easy loop, needed for each different num_comm
for i=1:num_valid
    tmp1=(month<month_valid(i));
    earl1_samegroup(i)=sum(tmp1.*index_group(:,group_valid(i)))*connectivity(group(i));
end

Xvalid=zeros(num_valid,num_para);
Xvalid(:,1)=1.0;
Xvalid(:,2)=log(earl1_sameauthor+1);
if(num_para>3)
    Xvalid(:,3)=earl1_importance/10^4;
    if(num_para>4)
        Xvalid(:,4)=log(earl1+1.0);
    end
end
Xvalid(:,num_para)=log(earl1_samegroup+1);

tmpeta1=Xvalid*theta;
ltmp=citefrom_valid'*tmpeta1-sum(exp(tmpeta1));
difftmp=norm(citefrom_valid-exp(tmpeta1));



%fprintf('loglikelihood in validation set: %e , improvement %f \n',ltmp, ltmp/logl_baseline(validation_set_number,2)-1); 
%fprintf('SSE in validation set: %e,improvement %f  \n',difftmp^2, 1-difftmp^2/sse_baseline(validation_set_number,2));
