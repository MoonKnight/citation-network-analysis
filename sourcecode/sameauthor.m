%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generate sameauthor matrix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

num_authors=0;
authorlist00=cell(10,1);

authormatrix00=sparse(num_nodes00,10);
authornumber00=zeros(num_nodes00,1);
tic;
for i=1:num_nodes00
    authornumber00(i)=authors00(i).num_authors;
    for j=1:authornumber00(i)
        if(strcmp('',authors00(i).names{j})==0)
            index_author=strcmp(authorlist00,authors00(i).names{j});
            if(nnz(index_author)>0)
                authormatrix00(i,index_author)=1;
            else
                num_authors=num_authors+1;
                authorlist00{num_authors}=authors00(i).names{j};
                authormatrix00(i,num_authors)=1;
            end
        end
    end
    if(mod(i,300)==0)
        fprintf('portion: %f, time %f s \n',i/num_nodes00,toc);
        fprintf('number of authors: %d \n',num_authors);
    end
end






 
%sameauthor00=sparse(num_nodes00,num_nodes00);
tic;

len=100;
I = zeros (len, 1) ;
J = zeros (len, 1) ;
X = zeros (len, 1) ;
ntriplets = 1 ;

 for i=1:num_nodes00
    
        tmparray=authormatrix00(i,:)*authormatrix00';
        tmplength=nnz(tmparray);
        if(nnz(tmparray)>0)
            [i0,j0,k0]=find(tmparray);
            newend=ntriplets + tmplength - 1;
%             sameauthor00(i,:)=tmparray/authornumber00(i);
%             sameauthor00(ii,i)=tmpvalue/authornumber00(ii);
                I(ntriplets:newend)=i;
                J(ntriplets:newend)=j0;
                X(ntriplets:newend)=k0/authornumber00(i);
                ntriplets=newend+1;
       end
       % end
    if(mod(i,300)==0)
        fprintf('portion: %f, time %f s \n',i/num_nodes00,toc);
    end
 end
sameauthor00=sparse(I,J,X,num_nodes00,num_nodes00);
toc

 