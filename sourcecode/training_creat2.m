authors=authors00(index_train);
wordcount=wordcount00(index_train,:);
wordcounttotal=sum(wordcount);
%num_tokens=sum(index_train);

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % too noisy if total number of appearance <min_count
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% min_count=10;
% tmpindex=(wordcounttotal>min_count);
% wordcounttotaltmp=wordcounttotal(tmpindex);
% wordcounttmp=wordcount(:,tmpindex);
% tokenlisttmp=tokenlist(tmpindex);
% V=nnz(tmpindex);

num_valid=nnz(index_valid);     %

month_valid=month00(index_valid);
year_valid=year00(index_valid);
id_valid=id00(index_valid);

authors_valid=authors00(index_valid);
wordcount_valid=wordcount00(index_valid,:);