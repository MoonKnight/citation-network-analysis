%*****************************************
%% INITIALIZATIONS
%*****************************************

%*****************************************
%intialize the list of author
%*****************************************
num_author=1;
authorlist=cell(num_author,1);
authorlist{1}='C. Ahn';

%*****************************************
%initialize the author matrix, at least one author per paper
%*****************************************
total_author_app=10;%num_nodes;
authorI=zeros(total_author_app,1);
authorJ=zeros(total_author_app,1);

%*****************************************
%intialize the list of tokens
%*****************************************
num_tokens=1;
tokenlist=cell(num_tokens,1);
tokenlist{1}='quantum';
keepindex=zeros(num_tokens,1);

%*****************************************
%initialize the author matrix, at least one author per paper
%*****************************************
total_word_app=10;%num_nodes;
wordcountI=zeros(total_word_app,1);
wordcountJ=zeros(total_word_app,1);
wordcountK=zeros(total_word_app,1);


%wordcount=sparse(num_nodes,10);
load('../data/matlab_format/ignore.mat');
iauthor=1;
itoken=1;

%*****************************************
%% MAIN LOOP
%*****************************************
for paperyear=1992:2003
    
    directory=num2str(paperyear);
    filenames=strcat('../data/original_files/abstracts/',directory,'/*.abs');
    
    docs=dir(filenames);
    size0=size(docs,1);
    
    tic;
    i=1;
    
    %*****************************************
    % loop for each year
    %*****************************************
    for i=1:size0
 %   for i=1:100
        %*****************************************
        % get the id and index
        %*****************************************
 
        tmpstring1=strcat('../data/original_files/abstracts/',directory,'/',docs(i).name);
        paperindex=pointer(sscanf(docs(i).name, '%7d.abs'));
%         if(year00(paperindex)~=paperyear)
%             display(paperindex)
%         end
        
        

        fid=fopen(tmpstring1);
        tmpcounter=0;
        while(tmpcounter<2)
            tmpline= fgetl(fid);
            if(strcmp(tmpline,'\\'))
                tmpcounter=tmpcounter+1;
            elseif(length(tmpline)>5)
                %*****************************************
                % Create author list.
                %*****************************************
                if(strcmp(tmpline(1:6),'Author'))
                    if(tmpline(7)=='s') % multiple authors
                        tmpstring=sscanf(tmpline,'Authors: %[^\n]');           
                    else
                        tmpstring=sscanf(tmpline,'Author: %[^\n]');
                    end
                        
                        
                    tmpnames=strsplit(tmpstring, {', ', ' and '}, 'CollapseDelimiters',true);
                    for jj=1:length(tmpnames)
                        authorI(iauthor)= paperindex;
                        index_author=strcmpi(tmpnames{jj},authorlist);
                        if(nnz(index_author)>0)
                            tmparray=(1:num_author);
                            tmpindex=tmparray(index_author);
                            authorJ(iauthor)=tmpindex;
                        else
                            num_author=num_author+1;
                            authorlist=[authorlist , tmpnames{jj}];
                            authorJ(iauthor)=num_author;
                        end
                        iauthor=iauthor+1;
                        
                        if(iauthor==total_author_app)
                            authorI=[authorI; zeros(1000,1) ];
                            authorJ=[authorJ; zeros(1000,1) ];
                            total_author_app=total_author_app+10000;
                        end
                        
                    end
                end
            end 
        end
        %*****************************************
        % create codebook for tokens
        %*****************************************
        tmpline = fgetl(fid);
        while(~strcmp(tmpline,'\\'))
            tmptokens=strsplit(tmpline, {',', ' ', '.'}, 'CollapseDelimiters',true);
            tmplength=length(tmptokens);
            for jj=1:tmplength
                if(nnz(strcmpi(ignore_list,tmptokens(jj)))==0) %check if the token is in the ignore list 
                    index_token=strcmpi(tokenlist,tmptokens(jj));
                    if(nnz(index_token)~=0)
                        % already in the token list
                        tmparray=(1:num_tokens);
                        tmpindex=tmparray(index_token);
                        wordcountJ(itoken)=tmpindex;
                    else
                        % not in the token list yet
                        wordcountJ(itoken)=num_tokens+1;
                        num_tokens=num_tokens+1;
                        tokenlist=[tokenlist, tmptokens{jj}];
                    end
                    wordcountI(itoken)=paperindex;
                    wordcountK(itoken)=1;
                    itoken=itoken+1;
                    
                    %*****************************************
                    % allocate more memory if it is going out of range
                    %*****************************************
                    if(itoken==total_word_app)
                        wordcountI=[wordcountI; zeros(1000,1) ];
                        wordcountJ=[wordcountJ; zeros(1000,1) ];
                        wordcountK=[wordcountK; zeros(1000,1) ];
                        total_word_app=total_word_app+1000;
                    end
                    
                end
            end
            tmpline = fgetl(fid);
        end
        
        fclose(fid);
        %*****************************************
        %for monitoring progress
        %*****************************************
        if(mod(i,200)==0)
            fprintf('%6d of %6d docs in year %d processed in %f seconds.\n', i, size0, paperyear, toc);
        end

   
    end
    
    fprintf('YEAR %d finished: %6d documents processed in %f seconds.\n',  paperyear,size0, toc);
    %*****************************************
    % about 70% of the tokens only appear once in this year
    % delete these tokens, assuming they are not important (at least in this
    % year)
    %*****************************************
    total_word_app=itoken-1;
    wordcountI=wordcountI(1:total_word_app);
    wordcountJ=wordcountJ(1:total_word_app);
    wordcountK=wordcountK(1:total_word_app);
    %current Wordcount_Matrix
    tmpmatrix=sparse(wordcountI,wordcountJ, wordcountK,num_nodes,num_tokens);
    
    
    tmpcount=sum(tmpmatrix);
    keepindex=(tmpcount>1);
    fprintf(' %6d tokens abandoned, %6d tokens left .\n',  num_tokens-nnz(keepindex),nnz(keepindex) );
    num_tokens=nnz(keepindex);
    tmpmatrix=tmpmatrix(:,keepindex);
    tokenlist=tokenlist(keepindex);
    
    
    [tmpI,tmpJ,tmpK]=find(tmpmatrix);
    wordcountI=tmpI;
    wordcountJ=tmpJ;
    wordcountK=tmpK;
    
    
    itoken=length(wordcountI)+1;
      
    wordcountI=[wordcountI; zeros(1000,1) ];
    wordcountJ=[wordcountJ; zeros(1000,1) ];
    wordcountK=[wordcountK; zeros(1000,1) ];
    total_word_app=total_word_app+1000;

    
    
%     wordcounttotal=sum(wordcount);
%     tmpindex=(wordcounttotal>1);
%     wordcounttotal=wordcounttotal(tmpindex);
%     wordcount=wordcount(:,tmpindex);
%     tokenlist=tokenlist(tmpindex);
%     num_tokens=nnz(tmpindex);
%    dtime(paperyear-1991)=toc
end
%%

total_author_app=iauthor-1;
authorI=authorI(1:total_author_app);
authorJ=authorJ(1:total_author_app);
Author_Matrix=logical(sparse(authorI,authorJ,ones(total_author_app,1),num_nodes,num_author));
Authorlist=authorlist;
Authornumbers=full(sum(Author_Matrix,2));

total_word_app=itoken-1;
wordcountI=wordcountI(1:total_word_app);
wordcountJ=wordcountJ(1:total_word_app);
Wordcount_Matrix=sparse(wordcountI,wordcountJ, ones(total_word_app,1),num_nodes,num_tokens);
Wordcount_Total=full(sum(Wordcount_Matrix));
Tokenlist=tokenlist;