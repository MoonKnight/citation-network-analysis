complex=importdata('k_clique_complex.out');
logcomplex=log10(complex);

p=polyfit(logcomplex(2:8,1),logcomplex(2:8,2),1);

xtarget=log10(21204);
target=[xtarget;1];
ytarget=p*target;

oneday=[log10(86400),log(86400)];
oneweek=oneday*7;
tempx=[min(logcomplex(:,1)),xtarget];

plot(logcomplex(:,1),logcomplex(:,2));
hold on
plot(xtarget,ytarget,'ro');
plot(tempx,oneday,'k-');
plot(tempx,oneweek,'k--');
hold off

legend('small size test', 'target size', 'one day', 'one week');
