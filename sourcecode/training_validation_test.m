%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%seperate the testing set, validation set and training set
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% portoin of test data
portion_test=0.1;
%k_fold cross validation
k_fold=5;
portion_valid=(1-portion_test)/k_fold;

random_selection=rand(num_nodes00,1);
tmp=mod(random_selection-portion_test,portion_valid);
groupid=floor((random_selection-portion_test-tmp)/portion_valid+0.1)+1;
%groupid: 
%0:    test set
%1~k_fold:   training  valid set

index_test=(groupid==0);

