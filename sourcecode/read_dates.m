%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%read in hep-th-slacdates
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fid=fopen('../data/original_files/hep-th-slacdates');

%tmpstring=fscanf(fid, '# cross-listed papers have ids 11<true_id>\n');
i=1;
while (~feof(fid));
    data(i,1:4)=fscanf(fid, '%d %d-%d-%d\n',[1 4]);
    i=i+1;    
end
num_nodes=i-1;
fclose(fid);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%the information of id and month (month=1~124)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
id=data(:,1);
year=data(:,2);
month=data(:,3);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% correct obviously misspecified date 
% Arxiv started at Feb. 1992, any date before that must be wrong
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for i=1:num_nodes
    if (year(i)<1992 || data(i,4)==1)
        year(i)=year(i-1);
        month(i)=month(i-1);
    end
end

month=(year-1992)*12+month;
month=month-min(month)+1;
num_month=max(month);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1)Store a mapping so that we can get to the node by its id quickly, without
% doing a search every time. 
% 2)Elemenate duplicate nodes at the same time.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Cleaning up is originally needed for the dataset from stanford website
% but turns out the data from kdd2003 website is fine
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
pointer=sparse(max(id),1);
i=1;
while (~(i>num_nodes))
    %if it is a duplicate node
    if(pointer(id(i))~=0)
        j=pointer(id(i));
        
        id(j:num_nodes-1)=id(j+1:num_nodes);
        month(j:num_nodes-1)=month(j+1:num_nodes);
        year(j:num_nodes-1)=year(j+1:num_nodes);
        id=id(1:num_nodes-1);
        month=month(1:num_nodes-1);
        year=year(1:num_nodes-1);
        num_nodes=num_nodes-1;
        
        pointer(pointer>j)=pointer(pointer>j)-1;
        i=i-1;
    end
    pointer(id(i))=i;
    i=i+1;
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% test whether id and pointer are paired correctly
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for (i=1:num_nodes)
    if(pointer(id(i))~=i)
        display(i,id(i),pointer(id(i)));
    end
end

