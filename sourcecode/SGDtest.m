% load('authors_information.mat');
% load('kdd2003_wholedata.mat');
% load('author_and_abs.mat');
% 
% validation_set_number=1;
% num_para=5;
% num_comm=2;
% 
% fprintf('Start creating training set on citations \n');
% training_create;
% fprintf('Start creating training set on authors and abstracts \n');
% training_creat2;
% 
% GLM_prepare;
% EM_clustering;
% index_group=sparse(num_nodes,num_comm);
% for gg=1:num_comm
%     index_group(:,gg)=(group==gg);
% end
% %easy loop, needed for each different num_comm
% for i=1:num_nodes
%     tmp=(month<month(i));
%     earlier_samegroup(i)=sum(tmp.*index_group(:,group(i)));
% end
% 
% Xmatrix=zeros(num_nodes,num_para);
% Xmatrix(:,1)=1.0;
% Xmatrix(:,2)=log(earlier_sameauthor+1);
% if(num_para>3)
%     Xmatrix(:,3)=earlier_importance/10^4;
%     if(num_para>4)
%         Xmatrix(:,4)=log(earlier+1.0);
%     end
% end
% Xmatrix(:,num_para)=log(earlier_samegroup+1);
% 
% fprintf('\n\n Starting GLM fitting:\n')
% tmpY=citefrom;
% 
% tic;
% test=Xmatrix(:,2:num_para);
% theta_fit=glmfit(test,citefrom,'poisson');
% tmpeta=Xmatrix*theta_fit;
% ltmp=citefrom'*tmpeta-sum(exp(tmpeta));
% difftmp=norm(citefrom-exp(tmpeta));
% toc
% 
theta0=rand(num_para,1);
stopcriteria=0;
theta=zeros(num_para,1);
fprintf('\n\n Starting SGD for GLM fitting:\n')

tstep=0.0002;
alpha=0.95;
tic;
for ite=1:101
    perm=randperm(num_nodes);
    %theta0=theta;
    iii=1;
    for i=1:num_nodes
        ii=perm(i);
        %later=(month<month(ii));
        %tmpgroup=group(ii);
        tmpY=citefrom(ii);
        %         tmpX(1)=1;
        %         tmpX(2)=log(earlier_samegroup(ii));
        %         tmpX(3)=earlier_sameauthor(ii);
        %         tmpX(4)=earlier_importance(ii)/10^4;
        %         tmpX(5)=log(earlier(ii)+0.5);
        tmpX=Xmatrix(ii,:);
        h=exp(tmpX * theta);
        %h=h./(h+1);
        gradient=tmpX' * (tmpY-h);
        tmpmag=abs(max(gradient))*tstep;
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % to avoid divergent steps
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        if(tmpmag>1)
            gradient=gradient/tmpmag;
            fprintf(' %d: GRADIENT TOO LARGE\n \n', ii);
        end
        theta=theta + tstep*gradient;
        
        
        %         if(mod(i,int16(num_nodes/10))==1)
        %             index00=(ite-1)*10+iii;
        %             thetahistory(:,index00)=theta;
        %             diff(index00)=norm(theta-theta0);
        %             theta0=theta;
        %             iii=iii+1;
        %
        %
        %
        %
        %         end
        
        
        %if(stopcriteria>5)
        %    fprintf('what?');
        %    break;
        %end
        
        
    end
    
    index00=ite;
    
    difftheta(index00)=norm(theta-theta0);
    tmpeta=Xmatrix*theta;
    ltmp=citefrom'*tmpeta-sum(exp(tmpeta));
    loglikelihood(index00)=ltmp;
    diff(index00)=norm(citefrom-exp(tmpeta));
    thetahistory(:,index00)=theta;
    
    if (mod(ite,10)==1)
    fprintf('iteration %d : theta=%f, %f\n',ite,theta(1),theta(2));%, theta(3));%,theta(4));%, theta(5));
    fprintf('likelihood: %e\n', ltmp);
    fprintf('tstep: %e, time since start %f s\n', tstep,toc);
    end
    
    if(ite>1)
              if(loglikelihood(index00)-loglikelihood(index00-1)<0.1)
                     stopcriteria=stopcriteria+1;
                 else
                     stopcriteria=0;
                 end
    end
    
    
    %tmptime=toc;
    %   fprintf('iteration %d : theta=%f, %f, %f, %f\n',ite,theta(1),theta(2), theta(3),theta(4));
    %   fprintf('tstep: %f, time since start %f s\n', tstep,toc);
    tstep=tstep*alpha;
    thetahistory(:,ite)=theta;
    if(stopcriteria>5)
        itefinal=ite;
        thetahistory=thetahistory(:,1:itefinal);
        diff=diff(1:itefinal);
        loglikelihood=loglikelihood(1:itefinal);
        break;
    end
    
end