complex=importdata('k_clique_complex.out');
logcomplex=log10(complex);

p=polyfit(logcomplex(2:8,1),logcomplex(2:8,2),1);

xtarget=log10(21204);
target=[xtarget;1];
ytarget=p*target;

oneday=[log10(86400),log10(86400)];
oneweek=oneday+log10(7);
tempx=[min(logcomplex(:,1)),xtarget];
tempx2=[logcomplex(2,1),xtarget];

tempy=[p*[tempx2(1);1], p*[tempx2(2);1]];


plot(logcomplex(:,1),logcomplex(:,2),'d-','Linewidth',2);
hold on
plot(tempx2,tempy,'r');
plot(xtarget,ytarget,'ro');
plot(tempx,oneday,'k-','Linewidth',2);
plot(tempx,oneweek,'k--','Linewidth',2);
hold off

legend('small size test', 'fit' ,'target size', 'one day', 'one week');
legend('Location','West');
xlabel('log10(number of nodes)');
ylabel('log10(time in seconds)');
