 load('authors_information.mat');
 load('kdd2003_wholedata.mat');
 load('author_and_abs.mat');
 load('kdd2003.mat');
% 
 threshold_range=20:10:70;

% 
 logl=zeros(length(threshold_range),2);
 sse=zeros(length(threshold_range),2);
 logl_baseline=zeros(max(groupid),2);
 sse_baseline=zeros(max(groupid),2);
 rel_log=zeros(length(threshold_range),2);
 rel_sse=zeros(length(threshold_range),2);
% 
% 
% %Try multiple random initializations,
% %because EM_clustering is sometimes unstable
 num_repeat=1;
% min_count=10;
% 
 num_para=5; %number of parameters in the model
 validation_set_number=1;
 min_count=10; %min number of counts that will be considererd in NB_train
% 
 for validation_set_number=1:5

     index_valid=(groupid==validation_set_number);
     index_train=((groupid-validation_set_number).*groupid~=0);

     %     
     fprintf('Start creating training set on citations \n');
     training_create;
     fprintf('Start creating training set on authors and abstracts \n');
     training_creat2;
%     
     GLM_prepare;
    
   
   for irepeat=1:num_repeat
        k_para=1;
        for threshold=threshold_range
            
            fprintf('\n *********\n Start Modularity,  threshold: %d \n *********\n', threshold);
            
            modularity_dir;
            group=group_mod;
            NB_train;
            NB_test;
            
            
            
            GLMtest;
            logl(k_para,1)=logl(k_para,1)+ltmp/num_repeat;
            sse(k_para,1)=sse(k_para,1)+difftmp^2/num_repeat;
            
            GLMpredict;
            logl(k_para,2)=logl(k_para,2)+ltmp/num_repeat;
            sse(k_para,2)=sse(k_para,2)+difftmp^2/num_repeat;
            
            k_para=k_para+1;
        end
     end
end
%output;