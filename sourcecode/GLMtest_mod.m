%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SGD to solve generalized linear regression
% logit(p)=beta_0 + beta_1 samegroup
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%load('EM_cluster4.mat', 'group');
%load('EM_cluster4.mat', 'num_comm');
%load('kdd2003.mat','num_nodes');
%load('kdd2003.mat','Cit_Matrix');
%load('kdd2003.mat','month');


index_group=sparse(num_nodes,num_comm);
for gg=1:num_comm
    index_group(:,gg)=(group==gg);
end
%easy loop, needed for each different num_comm
for i=1:num_nodes
    tmp=(month<month(i));
    earlier_samegroup(i)=sum(tmp.*index_group(:,group(i)))*connectivity(group(i));
end

citefrom=full(sum(Cit_Matrix'))';


theta=zeros(num_para,1);
%tmpY=zeros(num_nodes,1);
tmpX=zeros(1,num_para);
%thetahistory=zeros(num_para,itemax);
%difftheta=zeros(1,itemax);
%diff=zeros(1,itemax);
%loglikelihood=zeros(1,itemax);
%importance=log((citedtotal+0.5)./(num_month-month+0.5));
%importance=importance-min(importance);
%importance(citedtotal==0)=log(1/(num_month-month(citedtotal==0)+0.5));

Xmatrix=zeros(num_nodes,num_para);
Xmatrix(:,1)=1.0;
Xmatrix(:,2)=log(earlier_sameauthor+1);
if(num_para>3)
    Xmatrix(:,3)=earlier_importance/10^4;
    if(num_para>4)
        Xmatrix(:,4)=log(earlier+1.0);
    end
end
Xmatrix(:,num_para)=log(earlier_samegroup+1);

theta0=theta;
stopcriteria=0;

fprintf('\n\n Starting SGD for GLM fitting:\n')
tmpY=citefrom;

tic;
test=Xmatrix(:,2:num_para);
theta=glmfit(test,citefrom,'poisson');
tmpeta=Xmatrix*theta;
ltmp=citefrom'*tmpeta-sum(exp(tmpeta));
difftmp=norm(citefrom-exp(tmpeta));
toc
