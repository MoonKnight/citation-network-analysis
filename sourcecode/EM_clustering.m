%pre-calculating the log of factorial using stirling approximation
log_fact_k=(citedtotal+0.5).*(log(citedtotal)-1)+1.4189; %1.4189=0.5+0.5*log(2*pi)
log_fact_k(citedtotal<2)=0; %factorial(0)=factorial(1)=1
log_fact_z=sparse(num_nodes,num_month);
log_fact_z=(cited+0.5).*(log(cited)-1)+1.4189;
log_fact_z(cited<2)=0;
log_fact=log_fact_k-transpose(sum(transpose(log_fact_z)));


%total number of communities
%num_comm=20;

L=0;
Change=0;
%random initialization of the clustering (by random q matrix)
group=zeros(num_nodes,1);
group0=ones(num_nodes,1); %back up arrays for testing convergence

%q matrix - pseudo probability 
q=rand(num_nodes,num_comm);
for i=1:num_nodes;
    q(i,:)=q(i,:)/sum(q(i,:));  %sum_r(q_ir)=1;
    [tmp1,tmpint]=max(q(i,:));
    group(i)=tmpint;
end

%trivial initialization of pi_r and theta_r(t)
marg_prob=ones(num_comm,1)/num_comm;  %sum_r(pi_r)=1;
time_prob=ones(num_comm,num_month)/num_month;  %sum_t(theta_r(t))=1;
%some time_prob are too small to show by double precision,
%always use log_time_prob in the iteration instead.
log_time_prob=log(time_prob); 

%total likelihood
Ltotal=0;
Ltotal0=0;  %back up value for testing convergence



%solve optimization problem by EM iteration

ite=1;
while(nnz(group-group0)>num_comm/3)
% while(ite<15)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %Maximization Part
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    for r=1:num_comm
        marg_prob(r)=sum(q(:,r))/num_nodes;
        tmp=log(transpose(q(:,r))*citedtotal);
        for j=1:num_month
            %if the value is too small for double precision, set it to be e^-750
            log_time_prob(r,j)=max(log(transpose(q(:,r))*cited(:,j)),-750)-tmp;
        end
    end
    time_prob=exp(log_time_prob);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %Expectation Part
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    for r=1:num_comm
        q(:,r)=marg_prob(r);
    end
    %use log so the calculation can be implementd by matrix multiply
    logq=log(q)+cited*log_time_prob';
    %some q too small so that exp(q)=0 in double precision. Use the
    %relative value instead here.
    tmpmatrix=transpose(ones(num_comm,1)*max(transpose(logq)));
    logq=logq-tmpmatrix;
    q=exp(logq);
    group0=group;
    %normalization so that sum_r(q_ir)=1;
    for i=1:num_nodes
        q(i,:)=q(i,:)/sum(q(i,:));
        [tmp1,tmpint]=max(q(i,:));
        group(i)=tmpint;
    end
    % it is OK that one of the logq is -Inf, which means q_ir=0
    logq=log(q);
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %calculating total likelihood
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    Ltotal0=Ltotal;
    
    Ltotal=0;
    
    Ltotal = Ltotal + sum(transpose(q)*log_fact);
    Ltotal = Ltotal + sum(q*log(marg_prob));
    
    tmp=cited*transpose(log_time_prob);
    Ltotal = Ltotal + sum(sum(q.*tmp));
    
    L(ite)=Ltotal;
    Change(ite)=nnz(group-group0);
    
    ite=ite+1;
end

groupsize=zeros(1:num_comm,1);

for gg=1:num_comm
    groupsize(gg)=nnz(group==gg);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Plot convergence
%Plot relative change of topic
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% rel_portion=zeros(num_comm,num_month);
% for j=1:num_month
%     tmparray=time_prob(:,j).*marg_prob;
%     rel_portion(:,j)=tmparray/sum(tmparray);
% end
% linecolor=hsv(num_comm);
% hFig = figure(1);
% set(hFig, 'Position', [0 0 1000 400])
% 
% %relative change of topic
% subplot(1,2,1);
% %figure;
% hold on;
% for r=1:num_comm
%     plot(rel_portion(r,:),'color',linecolor(r,:));
% end
% hold off;
% 
% %convergence of iteration
% subplot(1,2,2);
% ite=size(Change,2);
% plotx=1:ite;
% ploty1=(Ltotal-L);
% ploty2=(Change);
% plotyy(plotx,ploty1,plotx,ploty2,'plot');
