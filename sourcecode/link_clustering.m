%input needed:
%Cit_Matrix: citation matrix

% %%%%%%%%%%%%%%%%%%%%
% % large size test
% % comment this block if you are using pre-defined Cit_Matrix
% %%%%%%%%%%%%%%%%%%%
%  
%  
%  load('kdd2003.mat','Cit_Matrix');

% %%%%%%%%%%%%%%%%%%%
% % small size test
% % comment this block if you are using pre-defined Cit_Matirx
% %%%%%%%%%%%%%%%%%%%%
% 
% I=[1,1,2,3,4,5,5,6,6,7,8,8,9,10,11,11,12,13,14,14,1,10];
% J=[6,3,6,6,6,10,11,11,13,13,14,9,15,17,17,18,18,16,15,16,7,11];
% K=ones(length(I),1);
% 
% sparseMatrix=sparse(I,J,K,18,18);
% test=full(sparseMatrix);
% 
% Cit_Matrix=test;
% 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% clean up Cit_Matirx
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 21 papers cite themselves ...
% 245 pair of papers cite each other ...
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
num_nodes=size(Cit_Matrix,1);

testMatrix=Cit_Matrix+Cit_Matrix';

testindex=(testMatrix==2);
[ii,jj,kk]=find(testindex);
tmpindex0=(ii<jj);
ii=ii(tmpindex0);
jj=jj(tmpindex0);
kk=kk(tmpindex0);

tmpMatrix1=sparse(ii,jj,kk,num_nodes,num_nodes);
testMatrix=Cit_Matrix-tmpMatrix1;
for i=1:num_nodes
    testMatrix(i,i)=0;
end

[basicI, basicJ,basicK]=find(testMatrix);

LinkMatrix=full(testMatrix+testMatrix');
%include the nodeitself for calculating inclusive neigherhood
for i=1:num_nodes
    LinkMatrix(i,i)=1;
end

num_link=length(basicI);

indexMatrix=sparse(basicI,basicJ,1:num_link,num_nodes,num_nodes);
indexMatrix=indexMatrix+indexMatrix';


% to make sure basicI<basicJ (easier for later maniupation)
tmpindex2=(basicI>=basicJ);
tmpvector0=basicI(tmpindex2);
basicI(tmpindex2)=basicJ(tmpindex2);
basicJ(tmpindex2)=tmpvector0;

fprintf('link_clustering step 1, time: %f\n',toc);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%Construcut link similarity matrix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% declare variables
tmpI=zeros(num_link,1);
tmpJ=zeros(num_link,1);
tmpK=zeros(num_link,1);

% size of inclusive neighbourhood
inclusive_neighbourhood=sum(LinkMatrix) ;

segment=floor(num_nodes/40);
index=0;
for i1=1:num_nodes
    if(mod(i1,segment)==1)
        fprintf('portion1: %f, portion2: %f, time:%f \n, ', i1/num_nodes, index/num_link, toc);
    end
    index_0=1:num_nodes;
    index_0=index_0(LinkMatrix(:,i1)==1);
    
    tmpsize=length(index_0);
    
    if(tmpsize==1)
        continue;
    end
    
    for j1=1:tmpsize
        if(index_0(j1)==i1)
            continue;
        end
            
        for j2=j1+1:tmpsize
            
            if(index_0(j2)==i1)
                continue;
            end
            
            doc1=index_0(j1);
            doc2=index_0(j2);
            
            index=index+1;
            tmpvector1=LinkMatrix(:,doc1);
            tmpvector2=LinkMatrix(:,doc2);
            intersect=nnz(tmpvector1.*tmpvector2);
            summation=inclusive_neighbourhood(doc1)+ inclusive_neighbourhood(doc2)-intersect;
            
            tmpI(index)=indexMatrix(i1,doc1);
            tmpJ(index)=indexMatrix(i1,doc2);
            tmpK(index)=intersect/summation;
            
        end
    end
    

        
    
    
end
 if(index<num_link)
     tmpI=tmpI(1:index);
     tmpJ=tmpJ(1:index);
     tmpK=tmpK(1:index);
 end
 
 fprintf('link_clustering step 2, time: %f\n',toc);
 
 
 num_linkpair=index;
 graphlength=num_linkpair;
 
 
 
 Link_Sim=sparse(tmpI,tmpJ,tmpK,num_link,num_link);
 
 [sortedK,sortedIndex]=sort(tmpK,'descend');
 linkI=tmpI(sortedIndex);
 linkJ=tmpJ(sortedIndex);
 linkK=tmpK(sortedIndex);
 
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 % dendrogram 
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
 tmpinteger=min(10,int32(num_linkpair/400));
 Dendrogram=zeros(num_link,tmpinteger);
 PartitionDensity=zeros(num_linkpair,1);
 %subsetsize=ones(1,num_linkpair);
 similaritylabel=zeros(num_linkpair,1);
 tmpgroupvector=1:num_link;
 possiblemaxima=zeros(1,tmpinteger);
 
 index=0;
 delta00=0;
 delta01=0;
 segment=floor(num_linkpair/10);
 
 fprintf('link_clustering step 3, time: %f\n',toc);
 for i=1:num_linkpair
     flag=0;
     %merge two groups with highest similarity
     
     if(mod(i,int32(num_linkpair/20))==1)
        fprintf('portion: %f, time:%f \n', i/num_linkpair, toc);
     end
     
     if(i>1)
         tempvector=tmpgroupvector;
         tempi=linkI(i-1);
         tempj=linkJ(i-1);
         similaritylabel(i)=linkK(i-1);
         tempigroup=tempvector(tempi);
         tempjgroup=tempvector(tempj);
         if(tempigroup==tempjgroup)
             Dendrogram(:,i)=tempvector;
             PartitionDensity(i)=PartitionDensity(i-1);
             continue;
         end
         tempgroup=min(tempigroup,tempjgroup);
         tempindexi=(tempvector==tempigroup);
         tempindexj=(tempvector==tempjgroup);
         tempvector(tempindexi)=tempgroup;
         tempvector(tempindexj)=tempgroup;         
     else
         tempvector=1:num_link;
         similaritylabel(i)=1.0;
     end
  
     
     tmpgroupvector=tempvector;
%      
%      if(flag==1)
%          PartitionDensity(i)=PartitionDensity(i-1);
%          continue;
%      end
         
     
     
     % calculate change in Partition Density
     if(i>1)
         m1=nnz(tempindexi);
         m2=nnz(tempindexj);
         
         
         temp1=zeros(2*m1,1);
         temp1(1:m1)=basicI(tempindexi);
         temp1(m1+1:2*m1)=basicJ(tempindexi);
         temp2=zeros(2*m2,1);
         temp2(1:m2)=basicI(tempindexj);
         temp2(m2+1:2*m2)=basicJ(tempindexj);
         
         
         n2=length(unique(temp2));
         n1=length(unique(temp1));
         
         temp0=zeros(2*m1+2*m2,1);
         temp0(1:2*m1)=temp1;
         temp0(2*m1+1:2*m2+2*m1)=temp2;
         n0=length(unique(temp0));
         
         if(n1>2)
            delta1=m1*(m1-n1+1)/(n1-2)/(n1-1)*2/num_link;
         else
             delta1=0;
         end
         
         if(n2>2)
             delta2=m2*(m2-n2+1)/(n2-2)/(n2-1)*2/num_link;
         else
             delta2=0;
         end
         delta0=(m1+m2)*(m1+m2-n0+1)/(n0-2)/(n0-1)*2/num_link;
         
         delta01=delta0-delta1-delta2;
         if((delta00>=0) && (delta01<0))
             index=index+1;
             Dendrogram(:,index)=tmpgroupvector;
             possiblemaxima(index)=i;
         end
         delta00=delta01;
         
         
         PartitionDensity(i)=PartitionDensity(i-1)-delta1-delta2+delta0;
     else
         PartitionDensity(i)=0.0;
     end
         
     if(nnz(tmpgroupvector>1)==0)
         graphlength=i;
         break;
     end
      

        
 end
 fprintf('link_clustering step 4, time: %f\n',toc);
 Dendrogram=Dendrogram(:,1:index);
 possiblemaxima=possiblemaxima(1:index);
 if(graphlength<num_linkpair)
    PartitionDensity=PartitionDensity(1:graphlength);
    similaritylabel=similaritylabel(1:graphlength);
 end
 
 
 
 plot(similaritylabel,PartitionDensity);
 
 
 
 
 
 