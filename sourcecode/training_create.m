%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%create training set
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%training set
num_nodes=nnz(index_train);     %
Cit_Matrix=Cit_Matrix00(index_train, index_train);

month=month00(index_train)+1;
year=year00(index_train);
id=id00(index_train);
num_month=max(month)-min(month)+1;
month=month-min(month)+1;

authors=authors00(index_train);
wordcount=wordcount00(index_train,:);
wordcounttotal=sum(wordcount);
%num_tokens=sum(index_train);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%crecreate citation map according to Cit_Matrix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
citefrom=zeros(num_nodes,1);
cited = sparse(num_nodes,max(month));
totalindex=1:num_nodes;
for toindex=1:num_nodes
    fromindex=totalindex(logical(Cit_Matrix(:,toindex)));
    size0=size(fromindex,2);
    
    for j=1:size0;
        month_cited=month(fromindex(j));
        cited(toindex,month_cited)=cited(toindex,month_cited)+1;
        citefrom(toindex)=citefrom(toindex)+1;
    end
end
citedtotal=transpose(sum(transpose(cited)));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1)Store a mapping so that we can get to the node by its id quickly, without
% doing a search every time. 
% 2)Elemenate duplicate nodes at the same time.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
pointer=sparse(max(id),1);
i=1;
while (~(i>num_nodes))
    %if it is a duplicate node
    if(pointer(id(i))~=0)
        j=pointer(id(i));
        
        id(j:num_nodes-1)=id(j+1:num_nodes);
        month(j:num_nodes-1)=month(j+1:num_nodes);
        year(j:num_nodes-1)=year(j+1:num_nodes);
        id=id(1:num_nodes-1);
        month=month(1:num_nodes-1);
        year=year(1:num_nodes-1);
        num_nodes=num_nodes-1;
        
        pointer(pointer>j)=pointer(pointer>j)-1;
        i=i-1;
    end
    pointer(id(i))=i;
    i=i+1;
end

disp(sprintf('Finished creating training set for the %d th validation set',validation_set_number))

%validation set
num_valid=nnz(index_valid);     %

month_valid=month00(index_valid);
year_valid=year00(index_valid);
id_valid=id00(index_valid);

authors_valid=authors00(index_valid);
wordcount_valid=wordcount00(index_valid,:);
