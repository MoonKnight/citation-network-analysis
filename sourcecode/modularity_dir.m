 %load('kdd2003.mat');
 
%  Cit_Matrix=[0 1 1; 1 1 0; 1 1 0];
%  num_nodes=3;
%  
%  
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 % create modularity matrix
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
 ModMatrix=Cit_Matrix;
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 % 21 papers cite themselves ...
 % 245 pair of papers cite each other ...
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
 for index0=1:num_nodes
     ModMatrix (index0,index0)=0.0;
 end
 
 fromtotal=full(sum(ModMatrix,2));
 citetotal=full(sum(ModMatrix));
 
 ModMatrix=full(ModMatrix);
 
 %ModMatrix(ModMatrix==2)=1;
 ModMatrix00=ModMatrix;
 %totallink=full(sum(ModMatrix));
 totalm = sum(fromtotal);
  
 ModMatrix=ModMatrix - (fromtotal*citetotal)/totalm;
 ModMatrix=ModMatrix;%/totalm;
 % 
 % testvector=rand(num_nodes,1);
 % %testvector0=ones(num_nodes,1);
 % 
 % diff_in_dir=100;
 % threshold=0.00000001;
 % itemax=100;
 % 
 % for ite=1:itemax
 %     testvector0=testvector;
 %     testvector=ModMatrix*testvector;
 %     diff_in_dir = norm(testvector/norm(testvector) - testvector0/norm(testvector0));
 %     fprintf('diff in iteration %d is %e.\n', ite, diff_in_dir);
 % end
 
ModMatrix0=ModMatrix+ModMatrix';
group_mod=ones(num_nodes,1);
groupsize=zeros(300,1);
num_comm=1;
%itemax=13;
nonseparable=zeros(300,1); 
 
ModMatrix=ModMatrix0;

num_groups=300;
groupsize=zeros(num_groups,1);
num_comm=1;
%itemax=13;
%num_groups=300;
nonseparable=zeros(num_groups,1); 
eigen0=zeros(num_groups,1);
smallgroup=zeros(num_groups,1);

k0=1;

tic;
while(sum(nonseparable)<num_comm)
    num_comm0=num_comm;
    for k=1:num_comm0
        if(nonseparable(k)==1)
            continue;
        end
        tmpindex0=(group_mod==k);
        [tmpI,tmpJ,tmpK]=find(tmpindex0);

        if(num_comm>1)
            tmpMatrix=ModMatrix00(tmpindex0,tmpindex0);
            tmpvector2=sum(tmpMatrix,2);
            tmpvector1=sum(tmpMatrix);
            tmptotal=sum(tmpvector1);
            %tmpd=sum(tmpvector1);
            %tmpvector0=tmpvector2-tmpvector1*tmpd/2/totalm
            if(tmptotal>0)
                tmpMatrix=tmpMatrix-tmpvector2*tmpvector1/tmptotal;
            end
            ModMatrix=(tmpMatrix+tmpMatrix');%/tmptotal;            
        end
 %       toc 
        
        if(num_comm>num_groups)
            num_groups=num_groups*2;
            nonseparable(num_groups/2:num_groups)=0;
            groupsize(num_groups/2:num_groups)=0;
            eigen0(num_groups/2:num_groups)=0;
            smallgroup(num_groups/2:num_groups)=0;
        end
            
 
        [tmpvector,tmpvalue]=eigs(ModMatrix,1,'la');
  %      toc
        
        tmpindex=(tmpvector<0);
   %     toc
   
        tmpcount=nnz(tmpindex);
        tmpcount=min(tmpcount,nnz(tmpindex0)-tmpcount);
        tmpcount2=nnz(tmpindex0)-nnz(tmpindex);
  %      threshold=10;
   
        
        if((tmpvalue>0.0) && (tmpcount>threshold))
            num_comm=num_comm+1;
            %tmpindex=(tmpvector<0);
            tmpI=tmpI(tmpindex);
            tmpJ=tmpJ(tmpindex);
            tmpK=tmpK(tmpindex);
            tmpindex2=sparse(tmpI,tmpJ,tmpK,num_nodes,1);
            group_mod(tmpindex2)=num_comm;
 %        fprintf('k: %d, num_comm: %d time: %f eigenvalue: %f size: %d, %d\n',k,num_comm,toc, tmpvalue,length(tmpI),tmpcount2);
        else
            nonseparable(k)=1;
            groupsize(k)=length(tmpI);
            if(~(tmpvalue>0.0))
                eigen0(k)=1;
            else
                smallgroup(k)=1;
            end
        end
        
    end
    k0=k0+1;
    fprintf('k0: %d, num_comm: %d time: %f\n',k0,num_comm,toc);%, tmpvalue,length(tmpI));   
end

num_groups=num_comm;
groupsize=groupsize(1:num_comm);
nonseparable=nonseparable(1:num_comm);
eigen0=eigen0(1:num_comm);
smallgroup=smallgroup(1:num_comm);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%intergroup connection
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

connectivity=zeros(num_comm,1);
for k=1:num_comm
    tmpindex=(group_mod==k);
    
    % within community links
    tmpMatrix=ModMatrix00(tmpindex,tmpindex);
    totallink_g=sum(sum(tmpMatrix));
        
    connectivity(k)=totallink_g*2/(groupsize(k)*(groupsize(k)-1));
end

fprintf('Size Limit: %d, Eigenvalue limit: %d \n', nnz(smallgroup),nnz(eigen0))