%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%train naive Baysien
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% gg=1;
% index_ingroup=(group==gg);
% index_outgroup=~index_ingroup;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% too noisy if total number of appearance <min_count
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%min_count=15;
tmpindex=(wordcounttotal>min_count);
wordcounttotaltmp=wordcounttotal(tmpindex);
wordcounttmp=wordcount(:,tmpindex);
tokenlisttmp=tokenlist00(tmpindex);
V=nnz(tmpindex);
totalcounttmp=sum(wordcounttotaltmp);

% neg=wordcounttmp(index_outgroup,:);
% pos=wordcounttmp(index_ingroup,:);
% 
% neg_words = sum(sum(neg));
% pos_words = sum(sum(pos));
% neg_log_prior = log(size(neg,1) / num_nodes);
% pos_log_prior = log(size(pos,1) / num_nodes);
% 
% % neg_log_phi=zeros(1,V);
% % pos_log_phi=zeros(1,V);
% 
% for k=1:V,
%     neg_log_phi(k) = log((sum(neg(:,k)) + 1) / (neg_words + V));
%     pos_log_phi(k) = log((sum(pos(:,k)) + 1) / (pos_words + V));
% end

groupMatrix=cell(2,num_comm);
num_words=zeros(2, num_comm);

log_prior=zeros(2,num_comm);
pos_log_phi=zeros(num_comm,V);
neg_log_phi=zeros(num_comm,V);
distinctivetoken=cell(num_comm,5);

for gg=1:num_comm
    groupMatrix{1,gg}=wordcounttmp(group_matrix(:,gg),:);
    groupMatrix{2,gg}=wordcounttmp(~group_matrix(:,gg),:);
    num_words(1,gg)=sum(sum(groupMatrix{1,gg}));
    num_words(2,gg)=sum(sum(groupMatrix{2,gg}));
    log_prior(1,gg)=log(size(groupMatrix{1,gg},1)/num_nodes);
    log_prior(2,gg)=log(size(groupMatrix{2,gg},1)/num_nodes);
    
    pos_log_phi(gg,:)=log((sum(groupMatrix{1,gg})+1)/(num_words(1,gg)+V));
    neg_log_phi(gg,:)=log((sum(groupMatrix{2,gg})+1)/(num_words(2,gg)+V));
    
    %log_neg = log((wordcounttotaltmp-sum(groupMatrix{gg})+1)/(totalcounttmp-num_words(gg)+V));
    diff=pos_log_phi(gg,:)-neg_log_phi(gg,:);
    
    if(groupsize(gg)==max(groupsize))
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %Display the 5 most distinctive words
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        [sortedValues,sortIndex] = sort(diff,'descend');
        maxIndex = sortIndex(1:5);
        %minIndex = sortIndex(end-4:end);
        
        distinctivetoken(gg,:)=tokenlisttmp(maxIndex);
        %tokenlisttmp(maxIndex)
        fprintf('the five most distinctive words for community: %d with size %d \n', gg, groupsize(gg));%,connectivity(gg));
        fprintf('%s, %s, %s, %s, %s\n',tokenlisttmp{maxIndex})
        tmpmatrix0(1,:)=wordcounttotaltmp(maxIndex);
        tmpmatrix0(2,:)=sum(groupMatrix{1,gg}(:,maxIndex));
        tmpmatrix0=full(tmpmatrix0);
        fprintf('%6d, %6d, %6d, %6d, %6d\n',tmpmatrix0(1,:))
        fprintf('%6d, %6d, %6d, %6d, %6d\n',tmpmatrix0(2,:))
    end
    % tokenlisttmp(minIndex)
    % diff(minIndex)
    % tmpmatrix(1,:)=wordcounttotaltmp(minIndex);
    % tmpmatrix(2,:)=sum(pos(:,minIndex));
    % full(tmpmatrix)
    %prob_ingroup=(sum(wordcount(index_ingroup))+1.0)/(number_ingroup+2.0);
    %prob_outgroup=(sum(wordcount(index_outgroup))+1.0)/(number_outgroup+2.0);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Prediction on Validation Set
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 testMatrix=wordcount00(index_valid,tmpindex);
 numTestDocs=nnz(index_valid);


