% use the results from link_clustering for node clustering

similar_short=similaritylabel(possiblemaxima);
pd_short=PartitionDensity(possiblemaxima);

[sortpd,sortIndex] = sort(pd_short,'descend');

topfive=sortIndex(1:5);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% clean up group id (from 1~ num_comm) and ignore groups with fewer than 
% (threshold_link=5) links together 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tmpvector=Dendrogram(:,index00);
tmpvector0=unique(tmpvector);
num_comm=length(tmpvector0);

linkgroup=ones(num_link,1);
%singlelink=logical(zeros(num_link,1));

threshold_link=5;
for gg=1:num_comm
    tmpindex=(tmpvector==tmpvector0(gg));
    n0=nnz(tmpindex);
    if(n0>threshold_link)
        linkgroup(tmpindex)=gg;
    else
        linkgroup(tmpindex)=0;
        %singlelink(tmpindex)=1;
    end
end

tmpvector1=unique(linkgroup(linkgroup>0));
tmpvector2=linkgroup;
num_comm=length(tmpvector1);
for gg=1:num_comm
    tmpindex=(linkgroup==tmpvector1(gg));
    tmpvector2(tmpindex)=gg;
end
linkgroup=tmpvector2;

tempI=[];
tempJ=[];
tempK=[];
groupsize=zeros(num_comm,1);


for gg=1:num_comm
    tmpindex_link=(linkgroup==gg);
    tmpvector3=unique([basicI(tmpindex_link); basicJ(tmpindex_link)]);
    groupsize(gg)=length(tmpvector3);
    tempI=[tempI; tmpvector3];
    tempJ=[tempJ; ones(groupsize(gg),1)*gg];
    tempK=[tempK; ones(groupsize(gg),1)];
    
end
tempK=logical(tempK);
group_matrix=sparse(tempI,tempJ,tempK,num_nodes,num_comm);






