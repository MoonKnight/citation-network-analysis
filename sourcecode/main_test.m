% Final test for different community detecting algorithms
% To test the link-community algorithm we only use 1/4 of the training
% data;


 load('authors_information.mat');
 load('kdd2003_wholedata.mat');
 load('author_and_abs.mat');
 load('kdd2003.mat');
 
 
 num_methods=3;
 % 1 for EM clustering
 % 2 for modularity clustering
 % 3 for directional-modularity clustering
 % 4 for link-clustering
 
 logl=zeros(num_methods,2);
 sse=zeros(num_methods,2);
 logl_baseline=zeros(1,2);
 sse_baseline=zeros(1,2);
 rel_log=zeros(num_methods,2);
 rel_sse=zeros(num_methods,2);

 validation_set_number=0;
 num_para=5;
 min_count=10;
 
 index_valid=(groupid==validation_set_number);
 index_filter=(mod(1:num_nodes00,4)==1);
 index_train=((groupid>0).*index_filter');
 index_train=logical(index_train);
 
 
 fprintf('Start creating training set on citations. Size: %d\n', nnz(index_train));
 training_create;
 fprintf('Start creating training set on authors and abstracts \n');
 training_creat2;
 
 
 GLM_prepare;
 predict0=exp(tmpeta1);
 logl_baseline(2) = citefrom_valid'*fgtmpeta1-sum(exp(tmpeta1));
 sse_baseline(2) = norm(citefrom_valid-exp(tmpeta1))^2;

 fit0=exp(tmpeta);
 logl_baseline(1) = citefrom'*tmpeta-sum(exp(tmpeta));
 sse_baseline(1) = norm(citefrom-exp(tmpeta))^2;
 
 fprintf('logl_baseline, train: %f, test: %f\n',logl_baseline(1),logl_baseline(2));
 fprintf('sse_baseline, train: %f, test: %f\n',sse_baseline(1),sse_baseline(2));
 theta_baseline
 k_para=1;
 num_repeat=1;
 
 num_comm=35;
 threshold1=40;
 threshold2=100;
 
 parameters=[nnz(index_train); threshold1; threshold2; num_comm; theta_baseline];
 p_summary=[p_summary,parameters];
 
 
 for method_index=1:3
 
     if(method_index==1)
         EM_clustering;
         connectivity=ones(num_comm,1);
     elseif(method_index==2)
         threshold=threshold1;
         modularity;
         group=group_mod;
     elseif(method_index==3)
         threshold=threshold2;
         modularity_dir;
         group=group_mod;
%     elseif(method_index==4)
%         link_clustering
     end
     

     
     
     connectivity=ones(num_comm,1);
     GLMtest_mod;
     %predict0=exp(tmpeta1);
     logl(k_para,1)=logl(k_para,1)+ltmp/num_repeat;
     sse(k_para,1)=sse(k_para,1)+difftmp^2/num_repeat;    
 
     NB_train;
     NB_test;     
     
     
     GLMpredict;
     fprintf('loglikelihood in validation set: %e , improvement %f \n',ltmp, ltmp/logl_baseline(2)-1); 
     fprintf('SSE in validation set: %e,improvement %f  \n',difftmp^2, 1-difftmp^2/sse_baseline(2));

     logl(k_para,2)=logl(k_para,2)+ltmp/num_repeat;
     sse(k_para,2)=sse(k_para,2)+difftmp^2/num_repeat;    
     k_para=k_para+1;
 end
 
 logl_sum=[logl_sum,[logl_baseline(2);logl(:,2)]];
 sse_sum=[sse_sum,[sse_baseline(2);sse(:,2)]];
 